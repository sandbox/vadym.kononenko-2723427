<?php
/**
 * @file
 * Display Suite Two Column Bricks template.
 *
 * Available variables:
 *
 * Layout:
 * - $classes: String of classes that can be used to style this layout.
 * - $contextual_links: Renderable array of contextual links.
 * - $layout_wrapper: wrapper surrounding the layout.
 *
 * Regions:
 *
 * - $header: Rendered content for the "Header" region.
 * - $header_classes: String of classes that can be used to style the "Header" region.
 *
 * - $above_left: Rendered content for the "Above Left" region.
 * - $above_left_classes: String of classes that can be used to style the "Above Left" region.
 *
 * - $above_right: Rendered content for the "Above Right" region.
 * - $above_right_classes: String of classes that can be used to style the "Above Right" region.
 *
 * - $above_brick: Rendered content for the "Above Brick" region.
 * - $above_brick_classes: String of classes that can be used to style the "Above Brick" region.
 *
 * - $middle_left: Rendered content for the "Middle Left" region.
 * - $middle_left_classes: String of classes that can be used to style the "Middle Left" region.
 *
 * - $middle_right: Rendered content for the "Middle Right" region.
 * - $middle_right_classes: String of classes that can be used to style the "Middle Right" region.
 *
 * - $below_brick: Rendered content for the "Below Brick" region.
 * - $below_brick_classes: String of classes that can be used to style the "Below Brick" region.
 *
 * - $below_left: Rendered content for the "Below Left" region.
 * - $below_left_classes: String of classes that can be used to style the "Below Left" region.
 *
 * - $below_right: Rendered content for the "Below Right" region.
 * - $below_right_classes: String of classes that can be used to style the "Below Right" region.
 *
 * - $footer: Rendered content for the "Footer" region.
 * - $footer_classes: String of classes that can be used to style the "Footer" region.
 */
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="two-column-double-bricks-fluid <?php print $classes;?> clearfix">

  <!-- Needed to activate contextual links. -->
  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

    <<?php print $header_wrapper; ?> class="ds-region ds-header ds-single<?php print $header_classes; ?>">
      <?php print $header; ?>
    </<?php print $header_wrapper; ?>>

    <?php
      $above_classes = '';
      // Add sidebar classes so that we can apply the correct width to the center region in css.
      if (($above_left && !$above_right) || ($above_right && !$above_left)) {
        $above_classes .= ' ds-single';
      }
    ?>
    <div class="ds-above ds-region-wrapper<?php print $above_classes; ?>">

    <?php if ($above_left): ?>
    <<?php print $above_left_wrapper; ?> class="ds-region ds-region-left<?php print $above_left_classes; ?>">
      <?php print $above_left; ?>
    </<?php print $above_left_wrapper; ?>>
    <?php endif; ?>

    <?php if ($above_right): ?>
    <<?php print $above_right_wrapper; ?> class="ds-region ds-region-right<?php print $above_right_classes; ?>">
      <?php print $above_right; ?>
    </<?php print $above_right_wrapper; ?>>
    <?php endif; ?>
    </div>

    <?php if ($above_brick): ?>
    <<?php print $above_brick_wrapper; ?> class="ds-region ds-above-brick ds-brick<?php print $above_brick_classes; ?>">
      <?php print $above_brick; ?>
    </<?php print $above_brick_wrapper; ?>>
    <?php endif; ?>

    <?php
      $middle_classes = '';
      // Add sidebar classes so that we can apply the correct width to the center region in css.
      if (($middle_left && !$middle_right) || ($middle_right && !$middle_left)) {
        $middle_classes .= ' ds-single';
      }
    ?>
    <div class="ds-middle ds-region-wrapper<?php print $middle_classes; ?>">
    <?php if ($middle_left): ?>
    <<?php print $middle_left_wrapper; ?> class="ds-region ds-region-left<?php print $middle_left_classes; ?>">
      <?php print $middle_left; ?>
    </<?php print $middle_left_wrapper; ?>>
    <?php endif; ?>

    <?php if ($middle_right): ?>
    <<?php print $middle_right_wrapper; ?> class="ds-region ds-region-right<?php print $middle_right_classes; ?>">
      <?php print $middle_right; ?>
    </<?php print $middle_right_wrapper; ?>>
    <?php endif; ?>
    </div>

    <?php if ($below_brick): ?>
    <<?php print $below_brick_wrapper; ?> class="ds-region ds-below-brick ds-brick<?php print $below_brick_classes; ?>">
      <?php print $below_brick; ?>
    </<?php print $below_brick_wrapper; ?>>
    <?php endif; ?>

    <?php
      $below_classes = '';
      // Add sidebar classes so that we can apply the correct width to the center region in css.
      if (($below_left && !$below_right) || ($below_right && !$below_left)) {
        $below_classes .= ' ds-single';
      }
    ?>
    <div class="ds-below ds-region-wrapper<?php print $below_classes; ?>">
    <?php if ($below_left): ?>
    <<?php print $below_left_wrapper; ?> class="ds-region ds-region-left<?php print $below_left_classes; ?>">
      <?php print $below_left; ?>
    </<?php print $below_left_wrapper; ?>>
    <?php endif; ?>

    <?php if ($below_right): ?>
    <<?php print $below_right_wrapper; ?> class="ds-region ds-region-right<?php print $below_right_classes; ?>">
      <?php print $below_right; ?>
    </<?php print $below_right_wrapper; ?>>
    <?php endif; ?>
    </div>

    <<?php print $footer_wrapper; ?> class="ds-region ds-footer ds-single<?php print $footer_classes; ?>">
      <?php print $footer; ?>
    </<?php print $footer_wrapper; ?>>

</<?php print $layout_wrapper ?>>

<!-- Needed to activate display suite support on forms. -->
<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
